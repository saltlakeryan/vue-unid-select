import Vue from 'vue'
import UnidSelect from './UnidSelect.vue'
import VueResource from 'vue-resource';

Vue.use(VueResource);

window.vue = new Vue({
  el: '#app',
  render: createElement => createElement(UnidSelect, {
    props: {
      url: "http://coe-admin.test/faculty-master-list/ldap-query",
      facultyOnly: true, 
      minimumSearchLength: 2
    }
  })
});
