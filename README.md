# vue-unid-select

> UNID input box

Use like so:
```html
    <unid-select 
        url="http://coe-admin.test/faculty-master-list/ldap-query" 
        data-store="faculty" 
        minimum-search-length="2" 
        label="Search for User"
        @input="onUpdate"/>
```

data-store can be "faculty" or "users" or "ldap"


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
